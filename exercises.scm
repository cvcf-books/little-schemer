;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Preface

(define (atom? x)
  (and (not (pair? x)) (not (null? x))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 02

;;;;
;; Exercise 01

(define (lat? xs)
  (cond ((null? xs) #t)
        ((atom? (car xs)) (lat? (cdr xs)))
        (else #f)))

;;;;
;; Exercise 02

;; lat? goes through the each item in the list and returns #t it gets to the end
;; of the list and has only come across atoms - otherwise, it returns #f.

;;;;
;; Exercise 03

;; See answer to Exercise 02.

;;;;
;; Exercise 04

;; (lat? l) returns #f when l is '(bacon (and eggs)) because once we've
;; processed (car l), and determined that it's an atom, we continue processing
;; (lat? (cdr l)) which does not pass the (atom? ...) test since (car (cdr l)) is
;; '(and eggs) which is a list and so is not an atom. As a result, (lat? l) must
;; return #f as its final result.

(define (member? x xs)
  (cond ((null? xs) #f)
        ((eq? x (car xs)) xs)
        (else (member? x (cdr xs)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 03

;;;;
;; Exercise 01

(define (rember x xs)
  (define (rec xs acc)
    (cond ((null? xs)       '())
          ((eq? x (car xs)) (append (reverse acc) (cdr xs)))
          (else             (rec (cdr xs) (cons (car xs) acc)))))
  (let ((ret (rec xs '())))
    (if (null? ret) xs ret)))

;;;;
;; Exercise 02

;;; My version
;; We went through each atom in xs and compared it to the atom, 'and. If (car xs)
;; is not 'and, then we recurse on the rest of the list and cons (car xs) onto
;; our accumulated list. If (car xs) is 'and, then we append the reversed
;; accumulated list to (cdr xs) - the rest of the items in xs that we have not
;; processed at that point - and return that. Finally, we check to see if the
;; returned value would be '() - the item was not found in xs - and, if so, return
;; the original list; otherwise, we return the accumulated list.

;;; Book version
;; We went through each atom in xs and compared it to the atom, 'and. If (car xs)
;; is not 'and, then we cons that value onto the value of (rember x (cdr xs)).
;; If it is 'and, then we return (cdr xs).

;;;;
;; Exercise 03 (book answer which isn't tail-recursive)

(define (rember2 x xs)
  (cond ((null? xs)       '())
        ((eq? x (car xs)) (cdr xs))
        (else             (cons (car xs) (rember x (cdr xs))))))

;;;;
;; Exercise 04

;; firsts returns a list of the first item in each of a list of lists.

;;;;
;; Exercise 05

(define (firsts xs)
  (define (rec xs acc)
    (cond ((null? xs) (reverse acc))
          (else       (rec (cdr xs) (cons (car (car xs)) acc)))))
  (rec xs '()))

(define (firsts2 xs)
  (cond ((null? xs) '())
        (else       (cons (car (car xs))
                          (firsts (cdr xs))))))

;;;;
;; Exercise 06

;; It goes through the list given as an argument - i.e. xs - and, upon finding
;; old in the list it inserts new after old and returns a new list with all the
;; values, including new.

;;;;
;; Exercise 07

(define (insertR new old xs)
  (define (rec xs acc)
    (cond ((null? xs)         (reverse acc))
          ((eq? old (car xs)) (append (reverse acc) (cons* old new (cdr xs))))
          (else               (rec (cdr xs) (cons (car xs) acc)))))
  (rec xs '()))

(define (insertR2 new old xs)
  (cond ((null? xs)         '())
        ((eq? old (car xs)) (cons* old new (cdr xs)))
        (else               (cons (car xs) (insertR new old (cdr xs))))))

;;;;
;; Exercise 08

(define (insertL new old xs)
  (define (rec xs acc)
    (cond ((null? xs)         (reverse acc))
          ((eq? old (car xs)) (append (reverse acc) (cons new xs)))
          (else               (rec (cdr xs) (cons (car xs) acc)))))
  (rec xs '()))

;;;;
;; Exercise 09

(define (subst new old xs)
  (define (rec xs acc)
    (cond ((null? xs)         (reverse acc))
          ((eq? old (car xs)) (append (reverse acc) (cons new (cdr xs))))
          (else               (rec (cdr xs) (cons (car xs) acc)))))
  (rec xs '()))

;;;;
;; Exercise 10

(define (subst2 new o1 o2 xs)
  (define (rec xs acc)
    (cond ((null? xs) (reverse acc))
          ((or (eq? o1 (car xs)) (eq? o2 (car xs)))
           (append (reverse acc) (cons new (cdr xs))))
          (else (rec (cdr xs) (cons (car xs) acc)))))
  (rec xs '()))

;;;;
;; Exercise 11

(define (multirember x xs)
  (define (rec xs acc)
    (cond ((null? xs)       (reverse acc))
          ((eq? x (car xs)) (append (reverse acc) (rec (cdr xs) '())))
          (else             (rec (cdr xs) (cons (car xs) acc)))))
  (rec xs '()))

;;;;
;; Exercise 12

(define (multiinsertR new old xs)
  (define (rec xs acc)
    (cond ((null? xs)         (reverse acc))
          ((eq? old (car xs)) (append (reverse acc) (cons* old new (rec (cdr xs) '()))))
          (else               (rec (cdr xs) (cons (car xs) acc)))))
  (rec xs '()))

;;;;
;; Exercise 13

(define (multiinsertL new old xs)
  (define (rec xs acc)
    (cond ((null? xs)         (reverse acc))
          ((eq? old (car xs)) (append (reverse acc) (cons* new old (rec (cdr xs) '()))))
          (else               (rec (cdr xs) (cons (car xs) acc)))))
  (rec xs '()))

;;;;
;; Exercise 14

(define (multisubst new old xs)
  (define (rec xs acc)
    (cond ((null? xs)         (reverse acc))
          ((eq? old (car xs)) (append (reverse acc) (cons new (rec (cdr xs) '()))))
          (else               (rec (cdr xs) (cons (car xs) acc)))))
  (rec xs '()))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 04

(define (add1 n)
  (+ n 1))

(define (sub1 n)
  (- n 1))

;;;;
;; Exercise 01

(define (add n m)
  (define (rec n acc)
    (cond ((zero? n) acc)
          (else      (rec (sub1 n) (add1 acc)))))
  (rec n m))

;;;;
;; Exercise 02

(define (sub n m)
  (define (rec m acc)
    (cond ((zero? m) acc)
          (else      (rec (sub1 m) (sub1 acc)))))
  (rec m n))

;;;;
;; Exercise 03

(define (addtup tup)
  (define (rec tup acc)
    (cond ((null? tup) acc)
          (else        (rec (cdr tup) (add (car tup) acc)))))
  (rec tup 0))

;;;;
;; Exercise 04

(define (times n m)
  (define (rec n acc)
    (cond ((zero? n) acc)
          (else      (rec (sub1 n) (add m acc)))))
  (rec n 0))

;;;;
;; Exercise 05

;; Note: If you want to stop after one of the lists runs out, replace the
;; (append (reverse acc) t*) with just a (reverse acc).
(define (tup+ t1 t2)
  (define (rec t1 t2 acc)
    (cond ((null? t1) (append (reverse acc) t2))
          ((null? t2) (append (reverse acc) t1))
          (else       (rec (cdr t1) (cdr t2)
                           (cons (add (car t1) (car t2)) acc)))))
  (rec t1 t2 '()))

;;;;
;; Exercise 06

;; Note: Only works on non-negative numbers!
(define (gt n m)
  (cond ((zero? n) #f)
        ((zero? m) #t)
        (else      (gt (sub1 n) (sub1 m)))))

;;;;
;; Exercise 07

;; Note: The better option would be (not (gt n m))...
(define (lt n m)
  (cond ((zero? m) #f)
        ((zero? n) #t)
        (else      (lt (sub1 n) (sub1 m)))))

;;;;
;; Exercise 08

(define (eq n m)
  (cond ((zero? n) (zero? m))
        ((zero? m) #f)
        (else      (eq (sub1 n) (sub1 m)))))

(define (eq n m)
  (cond ((lt n m) #f)
        ((gt n m) #f)
        (else     #t)))

;;;;
;; Exercise 09

(define (power b e)
  (define (rec e acc)
    (cond ((zero? e) acc)
          (else      (rec (sub1 e) (times b acc)))))
  (rec e 1))

;;;;
;; Exercise 10

(define (quot n m)
  (cond ((< n m) 0)
        (else    (add1 (quot (sub n m) m)))))

;; This function calculates a division so it should be called div, or something
;; like that name.

;;;;
;; Exercise 11

(define (len xs)
  (define (rec xs acc)
    (cond ((null? xs) acc)
          (else       (rec (cdr xs) (add1 acc)))))
  (rec xs 0))

;;;;
;; Exercise 12

(define (pick n xs)
  ;; Check (eq n 1) since they want n to be 1-based as opposed to 0-based.
  (cond ((eq n 1) (car xs))
        (else   (pick (sub1 n) (cdr xs)))))

;;;;
;; Exercise 13

(define (rempick n xs)
  (define (rec n xs acc)
    (cond ((one? n) (append (reverse acc) (cdr xs)))
          (else     (rec (sub1 n) (cdr xs) (cons (car xs) acc)))))
  (rec n xs '()))

;;;;
;; Exercise 14

(define (no-nums xs)
  (define (rec xs acc)
    (cond ((null? xs)         (reverse acc))
          ((number? (car xs)) (append (reverse acc) (rec (cdr xs) '())))
          (else               (rec (cdr xs) (cons (car xs) acc)))))
  (rec xs '()))

;;;;
;; Exercise 15

(define (all-nums xs)
  (define (rec xs acc)
    (cond ((null? xs)         (reverse acc))
          ((number? (car xs)) (rec (cdr xs) (cons (car xs) acc)))
          (else               (rec (cdr xs) acc))))
  (rec xs '()))

;;;;
;; Exercise 16

(define (eqan? a1 a2)
  (cond ((and (number? a1) (number? a2)) (eq a1 a2))
        ((or  (number? a1) (number? a2)) #f)
        (else                            (eq? a1 a2))))

;;;;
;; Exercise 17

(define (occur x xs)
  (define (rec xs acc)
    (cond ((null? xs)         acc)
          ((eqan? x (car xs)) (rec (cdr xs) (add1 acc)))
          (else               (rec (cdr xs) acc))))
  (rec xs 0))

;;;;
;; Exercise 18

(define (one? n)
  (= 1 n))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 05


;;;;
;; Exercise 01

(define (rember* x xs)
  (define (rec xs acc)
    (cond ((null? xs)       (reverse acc))
          ((eq? x (car xs)) (rec (cdr xs) acc))
          ((atom? (car xs)) (rec (cdr xs) (cons (car xs) acc)))
          (else             (rec (cdr xs) (cons (rec (car xs) '()) acc)))))
  (rec xs '()))

;;;;
;; Exercise 02

(define (insertR* new old xs)
  (define (rec xs acc)
    (cond ((null? xs)         (reverse acc))
          ((eq? old (car xs)) (rec (cdr xs) (cons* new old acc)))
          ((atom? (car xs))   (rec (cdr xs) (cons (car xs) acc)))
          (else               (rec (cdr xs) (cons (rec (car xs) '()) acc)))))
  (rec xs '()))

;;;;
;; Exercise 03

(define (occur* x xs)
  (define (rec xs acc)
    (cond ((null? xs)       acc)
          ((eq? x (car xs)) (rec (cdr xs) (add1 acc)))
          ((atom? (car xs)) (rec (cdr xs) acc))
          (else             (rec (cdr xs) (add (rec (car xs) 0) acc)))))
  (rec xs 0))

;;;;
;; Exercise 04

(define (subst* new old xs)
  (define (rec xs acc)
    (cond ((null? xs)         (reverse acc))
          ((eq? old (car xs)) (rec (cdr xs) (cons new acc)))
          ((atom? (car xs))   (rec (cdr xs) (cons (car xs) acc)))
          (else               (rec (cdr xs) (cons (rec (car xs) '()) acc)))))
  (rec xs '()))

;;;;
;; Exercise 05

(define (insertL* new old xs)
  (define (rec xs acc)
    (cond ((null? xs)         (reverse acc))
          ((eq? old (car xs)) (rec (cdr xs) (cons* old new acc)))
          ((atom? (car xs))   (rec (cdr xs) (cons (car xs) acc)))
          (else               (rec (cdr xs) (cons (rec (car xs) '()) acc)))))
  (rec xs '()))

;;;;
;; Exercise 06

(define (member* x xs)
  (cond ((null? xs)       #f)
        ((eq? x (car xs)) #t)
        ((atom? (car xs)) (member* x (cdr xs)))
        (else             (or (member* x (car xs)) (member* x (cdr xs))))))

;;;;
;; Exercise 07

(define (leftmost xs)
  (cond ((null? xs)      '())
        ((atom? (car xs)) (car xs))
        (else             (leftmost (car xs)))))

;;;;
;; Exercise 08

;; leftmost returns the first atom in the list when starting to search from the
;; left side.

;;;;
;; Exercise 09

(let ((x 'pizza)
      (l '(pizza pie)))
  (and (atom? (car l))
       (eq? (car l) x)))

;;;;
;; Exercise 10

;; and evaluates each of its s-expressions until one either returns false, in
;; which case and returns false, or it has evaluated all of its s-expressions
;; and none return false which yields a true result.

;;;;
;; Exercise 11

(define (eqlist? xs ys)
  (define (rec xs ys acc)
    (cond ((and (null? xs) (null? ys)) acc)
          ((or  (null? xs) (null? ys)) #f)
          (else                        (rec (cdr xs) (cdr ys)
                                            (and acc (equal? (car xs) (car ys)))))))
  (rec xs ys #t))

;;;;
;; Exercise 12

(define (equal? xs ys)
  (cond ((and (atom? xs) (atom? ys)) (eqan? xs ys))
        ((or  (atom? xs) (atom? ys)) #f)
        (else                        (eqlist? xs ys))))

;;;;
;; Exercise 13

(define (rember x xs)
  (define (rec xs acc)
    (cond ((null? xs)          (reverse acc))
          ((equal? x (car xs)) (append acc (cdr xs)))
          (else                (rec (cdr xs) (cons (car xs) acc)))))
  (rec xs '()))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 06

;;;;
;; Exercise 01

;; An arithmetic expression is any collection of numeric atoms and operations on
;; those atoms that result in a value, when evaluated.

;;;;
;; Exercise 02

(define (numbered? expr)
  (cond ((null? expr)                      #t)
        ((member? expr '(add times power)) #t)
        ((atom? expr)                      (number? expr))
        (else (and (numbered? (car expr)) (numbered? (cdr expr))))))

;; The book's version
;; (define (numbered? expr)
;;   (cond ((atom? expr) (number? expr))
;;         (else (and (numbered? (car expr)) (numbered? (car (cdr (cdr expr))))))))

;;;;
;; Exercise 03

;; I wanted to use:
;; ((member? (operator expr) '(add times power))
;;  (apply (operator expr) (list (value (1st-sub-exp expr)))
;;                               (value (2nd-sub-expr expr))))
;; but for some reason I'm having trouble calling apply like this (although I'm
;; pretty sure the equivalent in Common Lisp would have worked). I've got to
;; figure out why Scheme doesn't like this.
(define (value expr)
  (cond ((number? expr) expr)
        ((eq? (operator expr) 'add)
         (add (value (1st-sub-exp expr)) (value (2nd-sub-exp expr))))
        ((eq? (operator expr) 'times)
         (times (value (1st-sub-exp expr)) (value (2nd-sub-exp expr))))
        ((eq? (operator expr) 'power)
         (power (value (1st-sub-exp expr)) (value (2nd-sub-exp expr))))))

;;;;
;; Exercise 04

(define (1st-sub-exp expr)
  (car (cdr expr)))

;;;;
;; Exercise 05

(define (2nd-sub-exp expr)
  (car (cdr (cdr expr))))

;;;;
;; Exercise 06

(define (operator expr)
  (car expr))

;;;;
;; Exercise 07

;; For the infix notation versi5on:

;; (define (1st-sub-exp expr)
;;   (car expr))

;; (define (operator expr)
;;   (car (cdr expr)))

;;;;
;; Exercise 08

(define (sero? n)
  (null? n))

;;;;
;; Exercise 09

(define (edd1 n)
  (cons '() n))

;;;;
;; Exercise 10

(define (zub1 n)
  (cdr n))

;;;;
;; Exercise 11

(define (edd n m)
  (define (rec n acc)
    (cond ((sero? n) acc)
          (else      (rec (zub1 n) (edd1 acc)))))
  (rec n m))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 07

;;;;
;; Exercise 01

(define (set? xs)
  (cond ((null? xs) #t)
        ((member? (car xs) (cdr xs)) #f)
        (else (set? (cdr xs)))))

;;;;
;; Exercise 02

(define (makeset xs)
  (cond ((null? xs) '())
        ((member? (car xs) (cdr xs)) (makeset (cdr xs)))
        (else (cons (car xs) (makeset (cdr xs))))))

;;;;
;; Exercise 03

(define (makeset2 xs)
  (cond ((null? xs) '())
        (else (cons (car xs) (makeset2 (multirember (car xs) (cdr xs)))))))

;;;;
;; Exercise 04

;; makeset2 first checks whether xs is NIL and, if so, returns NIL. Otherwise,
;; it keeps the first item of xs - (car xs) - to be consed onto the final result
;; and recurses on (cdr xs) with the caveat that every other (car xs) value is
;; removed from (cdr xs).

;;;;
;; Exercise 05

(define (subset? xs ys)
  (cond ((null? xs) #t)
        (else       (and (member? (car xs) ys)
                         (subset? (cdr xs) ys)))))

;;;;
;; Exercise 06

(define (eqset? xs ys)
  (and (subset? xs ys) (subset? ys xs)))

;;;;
;; Exercise 07

(define (intersect? xs ys)
  (cond ((null? xs) #f)
        (else       (or (member? (car xs) ys)
                        (intersect? (cdr xs) ys)))))

;;;;
;; Exercise 08

(define (intersect xs ys)
  (cond ((null? xs) '())
        ((member? (car xs) ys) (cons (car xs) (intersect (cdr xs) ys)))
        (else (intersect (cdr xs) ys))))

;;;;
;; Exercise 09

(define (union xs ys)
  (makeset (append xs ys)))

;;;;
;; Exercise 10

;; It returns the set difference.

;;;;
;; Exercise 11

(define (intersectall xs)
  (cond ((null? (cdr xs)) (car xs))
        (else (intersect (car xs) (intersectall (cdr xs))))))

;;;;
;; Exercise 12

(define (a-pair? x)
  (cond ((atom? x)             #f)
        ((null? x)             #f)
        ((null? (cdr x))       #f)
        ((null? (cdr (cdr x))) #t)
        (else                  #f)))

;;;;
;; Exercise 13

(define (first p)
  (car p))

(define (second p)
  (car (cdr p)))

(define (build s1 s2)
  (list s1 s2))

;;;;
;; Exercise 14

(define (third xs)
  (car (cdr (cdr xs))))

;;;;
;; Exercise 15

(define (fun? rel)
  (set? (firsts rel)))

;;;;
;; Exercise 16

(define (revpair p)
  (build (second p) (first p)))

(define (revrel rel)
  (cond ((null? rel) '())
        (else (cons (revpair (car rel)) (revrel  (cdr rel))))))

;;;;
;; Exercise 17

(define (fullfun? fun)
  (fun? (revrel fun)))

;; The book wanted:
;; (define (fullfun? fun)
;;   (set? (seconds fun)))

;;;;
;; Exercise 18

(define (seconds xs)
  (define (rec xs acc)
    (cond ((null? xs) (reverse acc))
          (else       (rec (cdr xs) (cons (car (cdr (car xs))) acc)))))
  (rec xs '()))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 08

;;;;
;; Exercise 01

(define (rember-f-2 test? x xs)
  (cond ((null? xs) '())
        ((test? x (car xs)) (cdr xs))
        (else (cons (car xs) (rember-f-2 test? x (cdr xs))))))

;;;;
;; Exercise 02

(define (eq?-c x)
  (lambda (y)
    (eq? x y)))

;;;;
;; Exercise 03

(define eq?-salad (eq?-c 'salad))

;;;;
;; Exercise 04

(define (rember-f test?)
  (lambda (x xs)
    (cond ((null? xs) '())
          ((test? x (car xs)) (cdr xs))
          (else (cons (car xs) ((rember-f test?) x (cdr xs)))))))

;;;;
;; Exercise 05

;; (rember-f eq?) returns a function that takes an item and a list. The function
;; will return the list of xs except for all elements in xs that are eq? with x.

;;;;
;; Exercise 06

(define rember-eq? (rember-f eq?))

;;;;
;; Exercise 07

(define (insertL-f test?)
  (lambda (new old xs)
    (cond ((null? xs) '())
          ((test? old (car xs)) (cons* new old (cdr xs)))
          (else (cons (car xs) ((insertL-f test?) new old (cdr xs)))))))

;;;;
;; Exercise 08

(define (insertR-f test?)
  (lambda (new old xs)
    (cond ((null? xs) '())
          ((test? old (car xs)) (cons* old new (cdr xs)))
          (else (cons (car xs) ((insertR-f test?) new old (cdr xs)))))))

;;;;
;; Exercise 09

(define (insert-g side)
  (lambda (test?)
    (lambda (new old xs)
      (cond ((null? xs) '())
            ((test? old (car xs)) (append (if (eq? side 'left)
                                              (list new old)
                                              (list old new))
                                          (cdr xs)))
            (else (cons (car xs) (((insert-g side) test?) new old (cdr xs))))))))

;;;;
;; Exercise 10

(define (seqL new old xs)
  (cons* new old xs))

(define (seqR new old xs)
  (cons* old new xs))

;;;;
;; Exercise 11

(define (insert-g seq)
  (lambda (test?)
    (lambda (new old xs)
      (cond ((null? xs) '())
            ((test? old (car xs)) (seq new old (cdr xs)))
            (else (cons (car xs) (((insert-g seq) test?) new old (cdr xs))))))))

;;;;
;; Exercise 12

(define insertL (insert-g seqL))

;;;;
;; Exercise 13

(define insertR (insert-g seqR))

;;;;
;; Exercise 14

(define insertL
  ((insert-g (lambda (new old xs)
               (cons* new old xs)))
   eq?))

;;;;
;; Exercise 15

(define (seqS new old xs)
  (cons new xs))

;;;;
;; Exercise 16

(define subst ((insert-g seqS) eq?))

;;;;
;; Exercise 17

(define (atom-to-function x)
  (cond ((eq? x 'add) add)
        ((eq? x 'times) times)
        (else power)))

;;;;
;; Exercise 18

(define (value expr)
  (cond ((atom? expr) expr)
        (else ((atom-to-function (operator expr))
               (value (1st-sub-exp expr))
               (value (2nd-sub-exp expr))))))

;;;;
;; Exercise 19

(define (multirember-f test?)
  (lambda (x xs)
    (cond ((null? xs) '())
          ((test? x (car xs)) ((multirember-f test?) x (cdr xs)))
          (else (cons (car xs) ((multirember-f test?) x (cdr xs)))))))

;;;;
;; Exercise 20

(define multirember-eq? (multirember-f eq?))

;;;;
;; Exercise 21

(define (multiremberT test? xs)
  (cond ((null? xs) '())
        ((test? (car xs)) (multiremberT test? (cdr xs)))
        (else (cons (car xs) (multiremberT test? (cdr xs))))))

;;;;
;; Exercise 22

(define (multiinsertLR new oldL oldR xs)
  (cond ((null? xs) '())
        ((eq? oldL (car xs)) (cons* new oldL (multiinsertLR new oldL oldR (cdr xs))))
        ((eq? oldR (car xs)) (cons* oldR new (multiinsertLR new oldL oldR (cdr xs))))
        (else (cons (car xs) (multiinsertLR new oldL oldR (cdr xs))))))

;;;;
;; Exercise 23

(define (multiinsertLR&co new oldL oldR xs col)
  (cond ((null? xs) (col '() 0 0))
        ((eq? oldL (car xs)) (multiinsertLR&co new oldL oldR (cdr xs)
                                               (lambda (ys L R)
                                                 (col (cons* new oldL ys) (add1 L) R))))
        ((eq? oldR (car xs)) (multiinsertLR&co new oldL oldR (cdr xs)
                                               (lambda (ys L R)
                                                 (col (cons* oldR new ys) L (add1 R)))))
        (else (multiinsertLR&co new oldL oldR (cdr xs)
                                (lambda (ys L R)
                                  (col (cons (car xs) ys) L R))))))

;;;;
;; Exercise 24

(define (evens-only* xs)
  (cond ((null? xs) '())
        ((atom? (car xs)) (if (even? (car xs))
                              (cons (car xs) (evens-only* (cdr xs)))
                              (evens-only* (cdr xs))))
        (else (cons (evens-only* (car xs))
                    (evens-only* (cdr xs))))))

;;;;
;; Exercise 25

(define (evens-only*&co xs col)
  (cond ((null? xs) (col '() 1 0))
        ((atom? (car xs)) (if (even? (car xs))
                              (evens-only*&co (cdr xs)
                                              (lambda (ys prod sum)
                                                (col (cons (car xs) ys)
                                                     (times prod (car xs)) sum)))
                              (evens-only*&co (cdr xs)
                                              (lambda (ys prod sum)
                                                (col ys prod (add sum (car xs)))))))
        (else (evens-only*&co (car xs)
                              (lambda (ys prod1 sum1)
                                (evens-only*&co (cdr xs)
                                                (lambda (zs prod2 sum2)
                                                  (col (cons ys zs)
                                                       (times prod1 prod2)
                                                       (add sum1 sum2)))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 09

;;;;
;; Exercise 01

(define (looking x xs)
  (keep-looking x (pick 1 xs) xs))

(define (keep-looking x n xs)
  (cond ((number? n) (keep-looking x (pick n xs) xs))
        (else (eq? x n))))

;;;;
;; Exercise 02

(define (shift pair)
  (build (first (first pair))
         (build (second (first pair))
                (second pair))))

;;;;
;; Exercise 03

;; shift builds a new pair by taking the first item in (car pair) and consing it
;; onto the new list containing the second item in (car pair) and the second
;; part of the original pair - i.e. (cdr pair).

;;;;
;; Exercise 04

(define (length* xs)
  (cond ((null? xs) 0)
        ((atom? (car xs)) (add1 (length* (cdr xs))))
        (else (add (length* (car xs))
                   (length* (cdr xs))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 10

;;;;
;; Exercise 01

(define new-entry build)

;;;;
;; Exercise 02

(define (lookup-in-entry name entry entry-f)
  (lookup-in-entry-help name (first entry) (second entry) entry-f))

(define (lookup-in-entry-help name names values entry-f)
  (cond ((null? names) (entry-f))
        ((eq? (first names) name) (first values))
        (else (lookup-in-entry-help name (cdr names) (cdr values) entry-f))))

;;;;
;; Exercise 03

(define extend-table cons)

;;;;
;; Exercise 04

(define (lookup-in-table name table table-f)
  (cond ((null? table) (table-f))
        (else (lookup-in-entry name (car table)
                               (lambda (name)
                                 (lookup-in-table name (cdr table) table-f))))))

;;;;
;; Exercise 05

(define (expression-to-action e)
  (cond ((atom? e) (atom-to-action e))
        (else (list-to-action e))))

(define (atom-to-action e)
  (cond ((number? e)      *const)
        ((eq? e #t)       *const)
        ((eq? e #f)       *const)
        ((eq? e 'cons)    *const)
        ((eq? e 'car)     *const)
        ((eq? e 'cdr)     *const)
        ((eq? e 'null?)   *const)
        ((eq? e 'eq?)     *const)
        ((eq? e 'atom?)   *const)
        ((eq? e 'zero?)   *const)
        ((eq? e 'add1)    *const)
        ((eq? e 'sub1)    *const)
        ((eq? e 'number?) *const)
        (else             *identifier)))

;;;;
;; Exercise 06

(define (list-to-action e)
  (cond ((atom? (car e)) (cond ((eq? (car e) 'quote)  *quote)
                               ((eq? (car e) 'lambda) *lambda)
                               ((eq? (car e) 'cond)   *cond)
                               (else                  *application)))
        (else *application)))

;;;;
;; Exercise 07

(define (value e)
  (meaning e '()))

(define (meaning e table)
  ((expression-to-action e) e table))

;;;;
;; Exercise 08

(define *const
  (lambda (e table)
    (cond ((number? e) e)
          ((eq? e #t) #t)
          ((eq? e #t) #f)
          (else (build 'primitive e)))))

(define *quote
  (lambda (e table)
    (text-of e)))

(define text-of second)

;;;;
;; Exercise 09

(define *identifier
  (lambda (e table)
    (lookup-in-table e table initial-table)))

(define (initial-table name)
  (car '()))

(define *lambda
  (lambda (e table)
    (build 'non-primitive (cons table (cdr e)))))

;;;;
;; Exercise 10

(define table-of   first)
(define formals-of second)
(define body-of    third)

;;;;
;; Exercise 11

;; cond tests a sequence of conditions until one returns true, in which case
;; it's (cdr is evaluated and returned), or the last test is performed in which
;; case it returns #f.

;;;;
;; Exercise 12

(define (evcon lines table)
  (cond ((else? (question-of (car lines)))
         (meaning (answer-of (car lines)) table))
        ((meaning (question-of (car lines)) table)
         (meaning (answer-of (car lines)) table))
        (else (evcon (cdr lines) table))))

(define (else? e)
  (eq? 'else e))

(define question-of first)
(define answer-of   second)

;;;;
;; Exercise 13

(define *cond
  (lambda (e table)
    (evcon (cond-lines-of e) table)))

(define cond-lines-of cdr)

;;;;
;; Exercise 14

(define (evlis xs table)
  (cond ((null? xs) '())
        (else (cons (meaning (car xs) table)
                    (evlis (cdr xs) table)))))

;;;;
;; Exercise 15

(define *application
  (lambda (e table)
    (apply* (meaning (function-of e) table)
            (evlis (arguments-of e) table))))

(define function-of  car)
(define argumenst-of cdr)

;;;;
;; Exercise 16

(define (primitive? f)
  (eq? 'primitive (car f)))

(define (non-primitive? f)
  (eq? 'non-primitive (car f)))

;;;;
;; Exercise 17

(define (apply* fun vals)
  (cond ((primitive? fun) (apply-primitive (second fun) vals))
        ((non-primitive? fun) (apply-closure (second fun) vals))))

(define (apply-primitive name vals)
  (cond ((eq? name 'cons)
         (cons (first vals) (second vals)))
        ((eq? name 'car)
         (car (first vals)))
        ((eq? name 'cdr)
         (cdr (first vals)))
        ((eq? name 'null?)
         (null? (first vals)))
        ((eq? name 'eq?)
         (eq? (first vals) (second vals)))
        ((eq? name 'atom?)
         (:atom? (first vals)))
        ((eq? name 'zero?)
         (zero? (first vals)))
        ((eq? name 'add1)
         (add1 (first vals)))
        ((eq? name 'sub1)
         (sub1 (first vals)))
        ((eq? name 'number?)
         (number? (first vals)))))

(define :atom?
  (lambda (x)
    (cond ((atom? x) #t)
          ((null? x) #f)
          ((eq? (car x) 'primitive) #t)
          ((eq? (car x) 'non-primitive) #t)
          (else #f))))

;;;;
;; Exercise 18

(define (apply-closure closure vals)
  (meaning (body-of closure)
           (extend-table (new-entry (formals-of closure) vals)
                         (table-of closure))))
